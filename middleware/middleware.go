package middleware

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

//HandleResponse - Handle response for all service
func HandleResponse(c echo.Context, retCode int, isError bool, msg string, u interface{}) error {
	resp := new(Response)

	resp.Error = false
	resp.Message = msg
	resp.Data = u

	if isError {
		resp.Error = true
	}

	return c.JSON(retCode, resp)
}

// CheckValidToken - Check valid token
func CheckValidToken(c echo.Context) error {
	// // Get cookie with the name is token
	// tokenInCookie, err := c.Cookie("token")
	// if err != nil {
	// 	return err
	// }

	// // Get the JWT string from the cookie
	// tknStr := tokenInCookie.Value

	tknStr := c.Request().Header.Get("token")

	// Initialize a new instance of `Claims`
	claims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
		return JwtKey, nil
	})
	if !tkn.Valid || err != nil {
		return err
	}
	return nil
}
