package service

import (
	"log"
	"net/http"

	"time"

	"github.com/labstack/echo"
	"github.com/streamline-app-v2/middleware"
	"github.com/streamline-app-v2/model"
	repo "github.com/streamline-app-v2/repository/interface"
	I "github.com/streamline-app-v2/service/interface"

	"github.com/dgrijalva/jwt-go"
)

type (
	// UserService - Handle with User Repository
	UserService struct {
		UserRepo repo.IUserRepository
	}

	// LoginResponse - LoginResponse
	LoginResponse struct {
		UserInfo interface{}
		Token    string
	}
)

// NewUserService - NewUserService
func NewUserService(userRepo repo.IUserRepository) I.UserService {
	return &UserService{
		UserRepo: userRepo,
	}
}

// Login - User Login - New version with JWT
func (h *UserService) Login(c echo.Context) error {
	// Bind request body to struct User
	u := new(model.User)
	if err := c.Bind(u); err != nil {
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "Login Failed", nil)
	}

	// Find User with username and password
	user, err := h.UserRepo.FindUser(u)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusUnauthorized, true, "Unauthorized", nil)
	}

	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(180 * time.Minute)
	// Create the JWT claims, which includes the username and expiry time
	claims := &middleware.Claims{
		Username: u.Username,
		TimeNow:  time.Now().String(),
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(middleware.JwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		return middleware.HandleResponse(c, http.StatusInternalServerError, true, "", nil)
	}

	// Finally, we set the client cookie for "token" as the JWT we just generated
	// we also set an expiry time which is the same as the token itself
	cookie := new(http.Cookie)
	cookie.Name = "token"
	cookie.Value = tokenString
	cookie.Expires = expirationTime
	c.SetCookie(cookie)
	resObj := &LoginResponse{
		UserInfo: user,
		Token:    tokenString,
	}
	return middleware.HandleResponse(c, http.StatusOK, false, "Login success", resObj)
}

// SignUp - Sign Up
func (h *UserService) SignUp(c echo.Context) error {
	// Bind request body to struct User
	u := new(model.User)
	if err := c.Bind(u); err != nil {
		log.Println("Error while bind user. Detail:", err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed", nil)
	}

	// Check User Exist
	exist, err := h.UserRepo.UserExist(u)
	if err != nil {
		log.Println(err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed", nil)
	}

	if exist {
		log.Println("Can not sign up user. User Existed")
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed, User existed", nil)
	}

	// Create New User
	if err := h.UserRepo.CreateNewUser(u); err != nil {
		log.Println("Error while create new user. Detail:", err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed, can not create new user", nil)
	}

	// Create UserLessonTrack
	if err := h.UserRepo.CreateUserLessonTrack(u); err != nil {
		log.Println("Error while create user lesson track. Detail:", err)
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "SignUp Failed, can not create user lesson track", nil)
	}

	return middleware.HandleResponse(c, 200, false, "SignUp Success", nil)
}
