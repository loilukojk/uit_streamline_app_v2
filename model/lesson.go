package model

// Lesson - Lesson
type Lesson struct {
	tableName struct{} `pg:"lesson"`

	ID           int      `pg:"id,pk"`
	Name         string   `pg:"name"`
	Audio        string   `pg:"audio"`
	ImageContent []string `pg:"image_content,array"`
	LevelID      int      `pg:"level_id"`
}
