package service

import (
	"log"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/streamline-app-v2/middleware"
	repo "github.com/streamline-app-v2/repository/interface"
	I "github.com/streamline-app-v2/service/interface"
)

type (
	// LessonService - Handle with Lesson Repository
	LessonService struct {
		LessonRepo repo.ILessonRepository
	}
)

// NewLessonService - NewLessonService
func NewLessonService(LessonRepo repo.ILessonRepository) I.LessonService {
	return &LessonService{
		LessonRepo: LessonRepo,
	}
}

// GetLessonListByUserIDAndLevelID - Get lesson list by user id and level id
func (h *LessonService) GetLessonListByUserIDAndLevelID(c echo.Context) error {
	// Check Valid Token
	if err := middleware.CheckValidToken(c); err != nil {
		return middleware.HandleResponse(c, http.StatusUnauthorized, true, "Token Is Invalid", nil)
	}

	// Handler
	struserid := c.Param("userid")
	strlevelid := c.Param("levelid")

	userid, _ := strconv.Atoi(struserid)
	levelid, _ := strconv.Atoi(strlevelid)

	data, err := h.LessonRepo.GetLessonListByUserIDAndLevelID(userid, levelid)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusInternalServerError, true, "Get Lesson List Failed", nil)
	}

	return middleware.HandleResponse(c, 200, false, "Get Lesson List Success", data)
}

// GetLessonContentByID - Get Lesson Content By ID
func (h *LessonService) GetLessonContentByID(c echo.Context) error {
	// Check Valid Token
	if err := middleware.CheckValidToken(c); err != nil {
		return middleware.HandleResponse(c, http.StatusUnauthorized, true, "Token Is Invalid", nil)
	}

	// Handler
	strlessonid := c.Param("lessonid")
	lessonid, _ := strconv.Atoi(strlessonid)
	data, err := h.LessonRepo.GetLessonContentByID(lessonid)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusInternalServerError, true, "Get Lesson Content Failed", nil)
	}
	return middleware.HandleResponse(c, 200, false, "Get Lesson Content Success", data)
}

// GetQuestionListbyLessonID - Get Question List by Lesson ID
func (h *LessonService) GetQuestionListbyLessonID(c echo.Context) error {
	// Check Valid Token
	if err := middleware.CheckValidToken(c); err != nil {
		return middleware.HandleResponse(c, http.StatusUnauthorized, true, "Token Is Invalid", nil)
	}

	// Handler
	strlessonid := c.Param("lessonid")
	lessonid, _ := strconv.Atoi(strlessonid)
	data, err := h.LessonRepo.GetQuestionListbyLessonID(lessonid)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusInternalServerError, true, "Get Question List By LessonID Failed", nil)
	}
	return middleware.HandleResponse(c, 200, false, "Get Question List By LessonID Success", data)
}

// CheckAnswerByLessonID - Check Answer By LessonID
func (h *LessonService) CheckAnswerByLessonID(c echo.Context) error {
	// Check Valid Token
	if err := middleware.CheckValidToken(c); err != nil {
		return middleware.HandleResponse(c, http.StatusUnauthorized, true, "Token Is Invalid", nil)
	}

	// Handler
	strlessonid := c.Param("lessonid")
	lessonid, _ := strconv.Atoi(strlessonid)
	struserid := c.Param("userid")
	userid, _ := strconv.Atoi(struserid)
	// Bind request body to struct RouteStatisticsBody
	u := &middleware.ClientAnswer{}
	if err := c.Bind(u); err != nil {
		log.Println("Error at bind to struct while creating ClientAnswer struct in service layer")
		return middleware.HandleResponse(c, http.StatusBadRequest, true, "Failed to check client answers", nil)
	}
	// Check Answer
	data, err := h.LessonRepo.CheckAnswerByLessonID(userid, lessonid, u)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusInternalServerError, true, "Check Answer By LessonID Failed", nil)
	}
	// Update DB about the test
	err = h.LessonRepo.UpdateUserTest(userid, lessonid, data.CorrectedTotal)
	if err != nil {
		return middleware.HandleResponse(c, http.StatusInternalServerError, true, "Check Answer By LessonID Failed", nil)
	}
	return middleware.HandleResponse(c, 200, false, "Check Answer By LessonID Success", data)
}
