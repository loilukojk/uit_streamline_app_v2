package repository

import (
	"github.com/streamline-app-v2/middleware"
)

type (
	//LessonInfo - LessonInfo
	LessonInfo struct {
		ID   int    `pg:"id"`
		Name string `pg:"name"`
	}

	//UserLessonInfo - UserLessonInfo
	UserLessonInfo struct {
		tableName     struct{} `pg:"user_lesson_track"`
		ID            int      `pg:"id"`
		Name          string   `pg:"name"`
		UserID        int      `pg:"userid"`
		LevelID       int      `pg:"level_id"`
		LearnTimes    int      `pg:"learntimes"`
		LastTestDate  string   `pg:"lasttestdate"`
		LastTestPoint string   `pg:"lasttestpoint"`
	}
)

//GetLessonListByUserIDAndLevelID - GetLessonListByUserIDAndLevelID
func (h *LessonRepo) GetLessonListByUserIDAndLevelID(userid int, levelid int) (interface{}, error) {
	data, err := h.getLessonListByUserIDAndLevelID(userid, levelid)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// GetLessonContentByID - GetLessonContentByID
func (h *LessonRepo) GetLessonContentByID(lessonid int) (interface{}, error) {
	data, err := h.getLessonContentByID(lessonid)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// GetQuestionListbyLessonID - GetQuestionListbyLessonID
func (h *LessonRepo) GetQuestionListbyLessonID(lessonid int) (interface{}, error) {
	data, err := h.getQuestionListbyLessonID(lessonid)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// CheckAnswerByLessonID - CheckAnswerByLessonID
func (h *LessonRepo) CheckAnswerByLessonID(userid int, lessonid int, clientAnswer *middleware.ClientAnswer) (*middleware.CheckAnswer, error) {
	data, err := h.checkAnswerByLessonID(userid, lessonid, clientAnswer)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// UpdateUserTest -UpdateUserTest
func (h *LessonRepo) UpdateUserTest(userid int, lessonid int, point int) error {

	err := h.updateUserTest(userid, lessonid, point)
	if err != nil {
		return err
	}
	return nil
}
