package repository

import (
	"log"
	"time"

	"github.com/go-pg/pg"
	"github.com/streamline-app-v2/middleware"
	"github.com/streamline-app-v2/model"
	I "github.com/streamline-app-v2/repository/interface"
)

type (
	// LessonRepo - handle with DB connection
	LessonRepo struct {
		DB *pg.DB
	}
)

// NewLessonRepository - NewLessonRepository
func NewLessonRepository(db *pg.DB) I.ILessonRepository {
	return &LessonRepo{
		DB: db,
	}
}

// getLessonListByUserIDAndLevelID - getLessonListByUserIDAndLevelID
func (h *LessonRepo) getLessonListByUserIDAndLevelID(userid int, levelid int) (interface{}, error) {
	// create query strings
	columnExprStr := "lesson.id, lesson.name, lesson.level_id"
	columnExprStr += ", ult.userid, ult.learntimes, ult.lasttestdate, ult.lasttestpoint"
	joinStr := "LEFT JOIN user_lesson_track AS ult ON ult.lessonid = lesson.id"

	// query
	data := []UserLessonInfo{}
	err := h.DB.Model().Table("lesson").
		ColumnExpr(columnExprStr).
		Join(joinStr).
		Group("lesson.id", "lesson.name", "ult.userid", "level_id", "ult.learntimes", "ult.lasttestdate", "ult.lasttestpoint").
		Where("level_id = ? and ult.userid = ?", levelid, userid).
		//Order("numerical_order ASC").
		Select(&data)
	if err != nil {
		log.Println("Error while get lesson list by levelid and userid. Detail:", err)
		return nil, err
	}
	return data, nil
}

// getLessonContentByID - getLessonContentByID
func (h *LessonRepo) getLessonContentByID(lessonid int) (interface{}, error) {

	// query
	data := model.Lesson{}
	err := h.DB.Model().Table("lesson").
		Where("id = ?", lessonid).
		Select(&data)
	if err != nil {
		log.Println("Error while get lesson content by id. Detail:", err)
		return nil, err
	}
	return data, nil
}

// getQuestionListbyLessonID - getQuestionListbyLessonID
func (h *LessonRepo) getQuestionListbyLessonID(lessonid int) (interface{}, error) {

	// query
	data := []model.Question{}
	err := h.DB.Model().Table("question").
		Where("lesson_id = ?", lessonid).
		Select(&data)
	if err != nil {
		log.Println("Error while get question list by lessonid. Detail:", err)
		return nil, err
	}
	return data, nil
}

// checkAnswerByLessonID - checkAnswerByLessonID
func (h *LessonRepo) checkAnswerByLessonID(userid int, lessonid int, clientAnswer *middleware.ClientAnswer) (*middleware.CheckAnswer, error) {
	// Khởi tạo
	clAns := clientAnswer.ClientAnswer
	data := &middleware.CheckAnswer{}
	data.CheckAnswerContent = make([]middleware.CheckAnswerContent, len(clAns))
	corectCount := 0

	// Check
	for i := 0; i < len(clAns); i++ {
		answer := &[]model.Question{}
		if err := h.DB.Model().Table("question").
			Where("lesson_id = ? and id = ? and answer = ?", lessonid, clAns[i].QuestionID, clAns[i].Answer).
			Limit(1).
			Select(answer); err != nil {
			log.Println("Error while check client answer. Detail:", err)
			return nil, err
		}

		// check xem đúng đáp án không: Đúng là nếu answer được tìm thấy
		tempCheck := middleware.CheckAnswerContent{
			QuestionID: clAns[i].QuestionID,
		}
		if len(*answer) > 0 {
			corectCount++
			tempCheck.Correct = true
		} else {
			tempCheck.Correct = false
		}

		// Ghi nhận lại câu trả lời là đúng hoặc sai
		data.CheckAnswerContent[i] = tempCheck
	}

	// Ghi nhận lại tổng số câu trả lời đúng
	data.CorrectedTotal = corectCount

	return data, nil
}

// updateUserTest - updateUserTest
func (h *LessonRepo) updateUserTest(userid int, lessonid int, point int) error {
	// get current date
	currentDate := time.Now()
	currentDateFormat := string(currentDate.Format("2006-01-02"))

	// update DB
	if _, err := h.DB.Model().Table("user_lesson_track").
		//Set("lasttestdate = ? and lasttestpoint = ?", currentDateFormat, point).
		//Set("lasttestdate = ? and lasttestpoint = ?", "2019-10-10", point).
		Set("lasttestdate = ?", currentDateFormat).
		Set("lasttestpoint = ?", point).
		Where("userid = ? and lessonid = ?", userid, lessonid).
		Update(); err != nil {
		log.Println("Error while update user test. Detail:", err)
		return err
	}
	return nil
}
