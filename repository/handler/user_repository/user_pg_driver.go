package repository

import (
	"log"
	"strconv"

	"github.com/go-pg/pg"
	"github.com/streamline-app-v2/model"
	I "github.com/streamline-app-v2/repository/interface"
)

type (
	// UserRepo - handle with DB connection
	UserRepo struct {
		DB *pg.DB
	}
)

// NewUserRepository - NewUserRepository
func NewUserRepository(db *pg.DB) I.IUserRepository {
	return &UserRepo{
		DB: db,
	}
}

func (h *UserRepo) setupLesson() error {
	imgPath := "vendor/images/connection"
	audioPath := "vendor/audios/connection"
	level := "/level3"

	for i := 160; i <= 239; i++ {
		//create image path and audio path
		lessonName := strconv.FormatInt(int64(i-159), 10)
		imgLink := []string{
			imgPath + level + "_" + lessonName + "a",
			imgPath + level + "_" + lessonName + "b",
			imgPath + level + "_" + lessonName + "c"}
		imgLink[0] = imgPath + level + "_" + lessonName + "a"
		imgLink[1] = imgPath + level + "_" + lessonName + "b"
		imgLink[2] = imgPath + level + "_" + lessonName + "c"
		audioLink := audioPath + level + "_" + lessonName

		//create lesson
		lesson := &model.Lesson{}
		lesson.ID = i
		lesson.Audio = audioLink
		lesson.ImageContent = imgLink
		lesson.LevelID = 1
		lesson.Name = lessonName

		log.Println(lesson)

		//insert to table lesson
		err := h.DB.Insert(lesson)
		if err != nil {
			return err
		}
	}
	return nil
}

//findUser - Find user in table app_user
func (h *UserRepo) findUser(u *model.User) (interface{}, error) {
	user := &model.User{}
	err := h.DB.Model().Table("app_user").
		Where("username = ? and password = ?", u.Username, u.Password).
		Limit(1).
		Select(user)

	if err != nil {
		return nil, err
	}

	return user, nil
}

//userExist - Find if user u exist in table user or not
func (h *UserRepo) userExist(u *model.User) (bool, error) {
	exist, err := h.DB.Model().Table("app_user").Where("username = ?", u.Username).Exists()
	if err != nil {
		return false, err
	}
	return exist, nil
}

//createNewUser - create new user
func (h *UserRepo) createNewUser(u *model.User) error {
	err := h.DB.Insert(u)
	if err != nil {
		return err
	}
	return nil
}

//createNewUser - create new user
func (h *UserRepo) createUserLessonTrack(username string) error {

	//get user id
	userID := 0
	err := h.DB.Model().Table("app_user").Column("id").Where("username = ?", username).Limit(1).Select(&userID)
	if err != nil {
		return err
	}

	//create profile with userid
	for i := 1; i <= 239; i++ {

		//create user lesson track
		ult := &model.UserLessonTrack{}
		ult.LessonID = i
		ult.UserID = userID

		//insert to table lesson
		err := h.DB.Insert(ult)
		if err != nil {
			return err
		}
	}

	return nil
}
