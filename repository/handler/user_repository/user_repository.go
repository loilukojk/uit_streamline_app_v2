package repository

import (
	"errors"

	"github.com/streamline-app-v2/model"
)

//SetupLesson - SetupLesson
func (h *UserRepo) SetupLesson() error {
	err := h.setupLesson()
	if err != nil {
		return err
	}
	return nil
}

//FindUser - Find if user u exist in table user or not
func (h *UserRepo) FindUser(u *model.User) (interface{}, error) {
	user, err := h.findUser(u)
	if err != nil {
		return nil, errors.New("Error while login")
	}

	if user == nil {
		return nil, errors.New("Login failed, user does not exist")
	}

	return user, nil
}

//UserExist - check user exist
func (h *UserRepo) UserExist(u *model.User) (bool, error) {
	exist, err := h.userExist(u)
	if err != nil {
		return false, err
	}
	return exist, nil
}

//CreateNewUser - create new user
func (h *UserRepo) CreateNewUser(u *model.User) error {
	err := h.createNewUser(u)
	if err != nil {
		return err
	}
	return nil
}

//CreateUserLessonTrack - CreateUserLessonTrack
func (h *UserRepo) CreateUserLessonTrack(u *model.User) error {
	err := h.createUserLessonTrack(u.Username)
	if err != nil {
		return err
	}
	return nil
}
