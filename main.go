package main

import (
	"github.com/go-pg/pg"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	lessonRepo "github.com/streamline-app-v2/repository/handler/lesson_repository"
	userRepo "github.com/streamline-app-v2/repository/handler/user_repository"
	s "github.com/streamline-app-v2/service/handler"

	// interact with postgreSQL
	_ "github.com/lib/pq"
)

const (
	host     string = "localhost"
	port     string = "5432"
	user     string = "postgres"
	password string = "postgres123"
	dbname   string = "STREAMLINE_APP"
)

func main() {

	e := echo.New()

	// middleware cors of echo framework
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// CORS restricted
	// Allows requests from any `dotnetDoamin & permissionDomain` origin
	// with GET, PUT, POST or DELETE method.
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowCredentials: true,
	}))

	// Database connection
	opts := pg.Options{
		User:     user,
		Password: password,
		Database: dbname,
		Addr:     host + ":" + port,
	}
	db := pg.Connect(&opts)

	// Initialize Repository Layer
	userRepo := userRepo.NewUserRepository(db)
	lessonRepo := lessonRepo.NewLessonRepository(db)

	// Initialize Service Layer
	userService := s.NewUserService(userRepo)
	lessonService := s.NewLessonService(lessonRepo)

	// Routes
	e.POST("/login", userService.Login)
	e.POST("/signup", userService.SignUp)
	e.GET("/lesson-list/:userid/:levelid", lessonService.GetLessonListByUserIDAndLevelID)
	e.GET("/lesson-content/:lessonid", lessonService.GetLessonContentByID)
	e.GET("/question/:lessonid", lessonService.GetQuestionListbyLessonID)
	e.POST("/check-answer/:userid/:lessonid", lessonService.CheckAnswerByLessonID)

	// Start server
	e.Static("/static", "vendor")
	e.Logger.Fatal(e.Start(":1232"))
}
