package service

import "github.com/labstack/echo"

// UserService - UserService
type UserService interface {
	Login(c echo.Context) error
	SignUp(c echo.Context) error
}
